trigger ContactCreation on Account (after update) {
    List<Account> accList = Trigger.new;
    List<Contact> conList = new List<Contact>();
    for(Account a : accList){
        for(Integer i = 0; i<a.Number_Of_Contacts__c; i++){
            Contact c = new Contact(LastName='Number Contact'+i, AccountId=a.Id);
            conList.add(c);
        }    
    }
    if(conList.size() > 0){
        insert conList;
    }
}