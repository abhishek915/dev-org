trigger opptyUpdate2TestTrigger on Opportunity (before update) {
    
    for(Opportunity opps: Trigger.new){
        String optyId = opps.Id;
        
        System.debug('optyId>>>>>'+ optyId);
        String trackNo = FindPrimeNumber.getAccountName(optyId);
        
        System.debug('trackNo>>>>'+ trackNo);
        
        if(trackNo == null || trackNo.length() == 0){
            trackNo = '09876';
        }
        
        opps.TrackingNumber__c = trackNo;
        System.debug('records is ' + opps);
    }
        
}