trigger ContactCheckContactUpdate on Contact (before insert, before update) {
    List<Contact> conList = trigger.new;
    for(Contact c : conList){
        if(c.Email == 'insert@contact.com' || c.Email == null){
            c.Check_Contact__c = true;
        }
    }
}