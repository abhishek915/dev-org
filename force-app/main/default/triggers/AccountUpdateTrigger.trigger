/*trigger AccountUpdateTrigger on Account (before update) {
    for(Account acc : Trigger.new){
        acc.Name = acc.Name+Date.today();
    }
}

trigger AccountUpdateTrigger on Account (after insert) {
    List<Contact> contactList = new List<Contact>();
    List<Account> acctList = new List<Account>();
    
    for(Account acc : [SELECT Id, Name FROM Account WHERE Id IN: Trigger.new]){
        acc.Name = acc.Name+Date.today();
        acctList.add(acc);
        Contact con = new Contact(LastName = acc.Name, FirstName = 'Test', AccountId = acc.Id);
        contactList.add(con);
    }
    update acctList;
    insert contactList;
}*/

trigger AccountUpdateTrigger on Account (after insert) {
    for(Account acc : [Select Id, Name FROM Account WHERE Id IN: Trigger.new]){
        acc.Name = acc.Name+Date.today();
        update acc;
    }
}