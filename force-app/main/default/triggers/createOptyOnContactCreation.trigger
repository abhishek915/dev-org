trigger createOptyOnContactCreation on Contact (after insert) {
    List<Opportunity> optyList = new List<Opportunity>();
    
    for(Contact c : Trigger.new){
        Opportunity opp = new Opportunity(Name=c.Name+'   custom',
                                          AccountId=c.AccountId,
                                          StageName='Qualification',
                                          CloseDate=System.today()+370);
        optyList.add(opp);
    }
    System.debug('optyList--'+optyList);
    insert optyList;
}