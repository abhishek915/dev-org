trigger updateOptyAmount on Opportunity (before update){
    Id accountId = null;
    List<Opportunity> oppList = null;
    Decimal sumOfOpty = 0.0;
    for(Opportunity opp : Trigger.new){
        Opportunity oldOpty = Trigger.oldMap.get(opp.Id);
        
        if(opp.Name != oldOpty.Name){
        	opp.Amount = 25000;    
            accountId = opp.AccountId;
            
            String queryStatement = 'SELECT Id, (SELECT Amount FROM Opportunities) FROM Account WHERE Id =: accountId';
            Account accOpty = Database.query(queryStatement);
            
            oppList = accOpty.opportunities;
            for(Opportunity o : oppList){
                sumOfOpty = sumOfOpty+o.Amount;
            }
            
            System.debug('oppty Sum -->>'+sumOfOpty);
            
            accOpty.Sum_Of_Opportunities_Amount__c = sumOfOpty;
            update accOpty;
        }
    }
}