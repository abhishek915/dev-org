trigger ContactBeforeInsertBeforeUpdate on Contact (before insert, before update) {
    if(trigger.isInsert){
        for(Contact c : Trigger.new){
            c.Email = 'insert@contact.com';   
        }
    }
    if(trigger.isUpdate){
        for(Contact c : Trigger.new){
            c.Email = 'update@contact.com';   
        }
    }

}