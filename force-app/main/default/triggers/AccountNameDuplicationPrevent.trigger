trigger AccountNameDuplicationPrevent on Account (before insert, before update) {
    for(Account a : Trigger.new){
        List<Account> accList = [SELECT Name FROM Account WHERE Name =: a.Name];
        if(accList.size() > 0){
            a.Name.addError('Account Name Duplicate!');
        }
    }
}