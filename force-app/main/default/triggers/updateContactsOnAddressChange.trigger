trigger updateContactsOnAddressChange on Account (before update) {
    
    Map<Id, Account> accountMap = new Map<Id, Account>();
    
    for(Account acc : Trigger.new){
        if(Trigger.oldMap.get(acc.Id).Phone != acc.Phone){
            accountMap.put(acc.Id, acc);
        }
    }
    
    List<Contact> contactListToUpdate = new List<Contact>();
    
    for(Contact c : [SELECT Id, AccountId, Phone
                     FROM Contact 
                     WHERE AccountId IN :accountMap.keySet()]){
                         Account parentAcc = accountMap.get(c.AccountId);
                         c.Phone = parentAcc.Phone;
                         contactListToUpdate.add(c);
                     }
    Database.update(contactListToUpdate, false);
}