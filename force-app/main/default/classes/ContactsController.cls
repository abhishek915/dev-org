public class ContactsController {
	@AuraEnabled
    public static List<Contact> getContacts(){
        return [SELECT Id, Name, Email, Phone 
               FROM Contact];
    }
}