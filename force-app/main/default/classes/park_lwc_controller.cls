/**
 * @File Name          : park_lwc_controller.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 29/11/2019, 8:11:48 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/11/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class park_lwc_controller {
    @AuraEnabled(cacheable=true)
    public static List<String> getImageUrl(Id parkRecId){
        System.debug('parkRecId-->>'+parkRecId);
        List<String> urls =  new List<String>();
        for(Park_Sightings__c psRec : [Select Id, Animal__r.Image__c FROM Park_Sightings__c WHERE Park__c = :parkRecId]){
            urls.add(psRec.Animal__r.Image__c);
        }
        System.debug('urls-->>'+urls);
        
        return urls;
    }
}