public with sharing class getContactsLWC{
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactsMethod(String searchKey){
      String key = '%' + searchKey + '%';
      if(key.equals('%%')){
        return null;
      }else{
        return [SELECT Id, Name FROM Contact WHERE Name LIKE :key LIMIT 10];
      }
    }  
}