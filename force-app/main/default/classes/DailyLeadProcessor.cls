global class DailyLeadProcessor implements Schedulable{
    
    global void execute(SchedulableContext sc){
        List<Lead> leadWithEmptySource = [SELECT Id FROM Lead WHERE LeadSource = null LIMIT 200];
        for(Lead l : leadWithEmptySource){
            l.LeadSource = 'Dreamforce';
        }

        update leadWithEmptySource;
    }

}