global class LeadProcessor implements Database.Batchable<sObject>, Database.Stateful  {
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT ID FROM Lead');
    }
    
    global void execute(Database.BatchableContext bc, List<Lead> leadRecords){
        // process each batch of records
        for (Lead lead : leadRecords) {
            lead.LeadSource = 'Dreamforce';
            recordsProcessed = recordsProcessed + 1;
        }
        update leadRecords;
    }    
    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed. Shazam!');
    }   
}