public class AccountFieldSetTest_Class {
    
    @AuraEnabled
    public static List<Account> getFieldSetFields(Id accountId){
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Account');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('Accounts_Field_Set');
        
        String[] fieldSetFields = new List<String>();
        
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetObj.getFields()){
            System.debug(fieldSetMemberObj.getFieldPath());
            fieldSetFields.add(fieldSetMemberObj.getFieldPath());
        }
        
        System.debug('fieldSetFields-->>'+fieldSetFields);
        
        String s = 'SELECT ' + String.join(fieldSetFields, ', ') + ' FROM Account WHERE Id = :accountId AND Website != null';
        System.debug('s-->>'+s);
        List<Account> accList = Database.query(s);
        System.debug('accList-->>'+accList);
        
        return accList;
    }
}