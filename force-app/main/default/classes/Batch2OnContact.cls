global class Batch2OnContact implements Schedulable{
    
    global void execute(SchedulableContext bc){
        String fN = 'Batch';
        String query = 'SELECT Id FROM Contact WHERE FirstName LIKE :fN';
        
        Batch1OnContact bc18 = new Batch1OnContact(query);
         
        Database.executeBatch(bc18);
    }
}