@isTest
private class AccountProcessorTest {
    @testSetup static void setup(){
        List<Account> accList = new List<Account>();
        for(Integer i = 0; i<5; i++){
            Account a = new Account(Name='Test Account--'+i);
            accList.add(a);
        }
        insert accList;
    }
    
    @isTest static void testMethod1(){
        Test.startTest();
        List<Id> accountIds = new List<Id>();
        List<Account> accList1 = [SELECT Id FROM Account WHERE Name LIKE 'Test Account--%'];
        for(Account acc1 : accList1){
            accountIds.add(acc1.Id);
        }
        AccountProcessor.countContacts(accountIds);
        Test.stopTest();
    }
}