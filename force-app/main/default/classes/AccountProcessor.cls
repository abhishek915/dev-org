public class AccountProcessor {
    
    @future
    public static void countContacts(List<Id> accountIds){
        List<Account> accList = [SELECT Name, Number_of_Contacts__c FROM Account WHERE Id IN :accountIds];
        for(Account acc : accList){
            Integer contactCount = [SELECT count() FROM Contact WHERE AccountId=:acc.Id];
            acc.Number_Of_Contacts__c = contactCount;
        }
        
        update accList;
    }

}