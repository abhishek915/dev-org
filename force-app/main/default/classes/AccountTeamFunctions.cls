public class AccountTeamFunctions {
    
    @AuraEnabled
    public static List<User> getAccountTeamMembers(Id accountId){
        String[] userIdList = new List<String>();
        List<User> userRecords = new List<User>();
        List<AccountTeamMember> associatedUserIdList = new List<AccountTeamMember>();
        associatedUserIdList = [SELECT UserId FROM AccountTeamMember WHERE AccountId=: accountId];
        
        for(AccountTeamMember associatedId : associatedUserIdList){
            userIdList.add(associatedId.UserId);
            User userRecord = getUserDetails(associatedId.UserId);
            userRecords.add(userRecord);
        }
        
        addUsersToNewOpty(accountId, userIdList);
        
        return userRecords;
    }
    
    @AuraEnabled
    public static void addUsersToNewOpty(Id accountId, List<String> userIdList){
        String [] optyIds = queryOpportunities(accountId);
        if(!optyIds.isEmpty()){
            for(String optyId : optyIds){
                String stateMent = 'SELECT UserId FROM OpportunityTeamMember WHERE OpportunityId =\''+ optyId+'\'';
                List<OpportunityTeamMember> optyTeamMem = Database.query(stateMent);
                //When opty team member is less than the account team member
                if(optyTeamMem.size() != userIdList.size()){
                    for(String userId : userIdList){
                        for(OpportunityTeamMember otmm : optyTeamMem){
                            if(!otmm.UserId.equals(userId)){
                                addUserToOpportunityTeam(optyId, userId);                        
                            }
                        }
                    }
                }
                //When no opty team member is there
                if(optyTeamMem.size() == 0){
                    for(String userId : userIdList){
                        addUserToOpportunityTeam(optyId, userId);                        
                    }                    
                }
            }
        }
    }
    
    @AuraEnabled
    public static User getUserDetails(Id userId){
        User userRecord = new User();
        userRecord = [SELECT User.FirstName, User.LastName, Profile.Name, Phone, UserRole.Name FROM User WHERE Id=: userId];
        return userRecord;
    }
    
    @AuraEnabled
    public static List<User> getAllUsers(){
        List<User> allUsersList = [SELECT User.LastName, User.FirstName, Profile.Name, UserRole.Name, Email, Manager.Name FROM User];
        return allUsersList;
    }
    
    @AuraEnabled
    public static boolean addUserToAccountTeam(Id accountId, Id userId){
        boolean addSuccess = false;
        AccountTeamMember accTeam = new AccountTeamMember(AccountId = accountId,
                                                          UserId = userId,
                                                          AccountAccessLevel = 'Edit',
                                                          ContactAccessLevel = 'Edit',
                                                          OpportunityAccessLevel = 'Edit');
        
        Database.SaveResult saveUserAccountRelation = Database.insert(accTeam, false);
        system.debug(saveUserAccountRelation.isSuccess());
        
        if(saveUserAccountRelation.isSuccess() == true){
            String [] optyIds = queryOpportunities(accountId);
            if(!optyIds.isEmpty()){
                for(String optyId : optyIds){
                    addSuccess = addUserToOpportunityTeam(optyId, userId);
                }
            }
        }
        return addSuccess;
    }
    
    @AuraEnabled
    public static List<String> queryOpportunities(Id accountId){
        String[] optyIdList = new List<String>();
        List<Opportunity> optyList = new List<Opportunity>();
        optyList = [SELECT Id FROM Opportunity WHERE AccountId =: accountId ];
        
        for(Opportunity opty : optyList){
            optyIdList.add(opty.Id);
        }
        return optyIdList;
    }
    
    @AuraEnabled
    public static boolean addUserToOpportunityTeam(Id optyId, Id userId){
        OpportunityTeamMember optyTeamMem = new OpportunityTeamMember( OpportunityId = optyId,
                                                                      UserId = userId,
                                                                      OpportunityAccessLevel = 'Edit');
        Database.SaveResult saveUserOptyRelation = Database.insert(optyTeamMem, false);
        system.debug(saveUserOptyRelation.isSuccess());
        return saveUserOptyRelation.isSuccess();
    }
    
    @AuraEnabled
    public static boolean deleteUserFromAccountTeam(Id accountId, Id userId){
        boolean deletionSuccess = false;
        AccountTeamMember accRel = [SELECT UserId FROM AccountTeamMember WHERE AccountId =: accountId AND UserId =: userid];
        Database.DeleteResult deleteResults = Database.delete(accRel, false);
        system.debug(deleteResults.isSuccess());
        if(deleteResults.isSuccess() == true){
            String [] optyIds = queryOpportunities(accountId);
            if(!optyIds.isEmpty()){
                for(String optyId : optyIds){
                    deletionSuccess = deleteUserFromOptyTeam(optyId, userId);
                }
            }
        }
        return deletionSuccess;
    }
    
    @AuraEnabled
    public static boolean deleteUserFromOptyTeam(Id optyId, Id userId){
        OpportunityTeamMember userOptyRel = [SELECT UserId FROM OpportunityTeamMember WHERE OpportunityId =: optyId AND UserId =: userid];
        Database.DeleteResult deleteResults = Database.delete(userOptyRel, false);
        system.debug(deleteResults.isSuccess());
        return deleteResults.isSuccess();
    }
    
    @AuraEnabled
    public static List<User> getUsersUsingFilter(String field, String filter, String keyword){
        String metaChar = '%';
        String searchKeyword = '';
        List<User> filteredUserList = new List<User>();
        
        if(field.equals('Last Name')){
            field = 'LastName';
        }
        if(field.equals('First Name')){
            field = 'FirstName';
        }
        if(field.equals('Job Title')){
            field = 'Profile.Name';
        }
        
        if(filter.equals('Begins With')){
            
            searchKeyword = keyword+metaChar;
            
            String queryStatement = 'SELECT User.LastName, User.FirstName, Profile.Name, UserRole.Name, Email, Manager.Name FROM USER WHERE ' + field + ' LIKE \'' + searchKeyword + '\'' ;
            filteredUserList = Database.query(queryStatement);
            system.debug('>>>'+filteredUserList);
        }
        if(filter.equals('Equals To')){
            searchKeyword = keyword;
            
            String queryStatement = 'SELECT User.LastName, User.FirstName, Profile.Name, UserRole.Name, Email, Manager.Name FROM USER WHERE ' + field + ' LIKE \'' + searchKeyword + '\'' ;
            filteredUserList = Database.query(queryStatement);
            system.debug('>>>'+filteredUserList);
        }
        if(filter.equals('Contains')){
            searchKeyword = metaChar+keyword+metaChar;
            
            String queryStatement = 'SELECT User.LastName, User.FirstName, Profile.Name, UserRole.Name, Email, Manager.Name FROM USER WHERE ' + field + ' LIKE \'' + searchKeyword + '\'' ;
            filteredUserList = Database.query(queryStatement);
            system.debug('>>>'+filteredUserList);
        }
        
        return filteredUserList;
    }
}