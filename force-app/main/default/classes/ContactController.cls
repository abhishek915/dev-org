/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-03-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial 2 Version
**/
public with sharing class ContactController {
  @AuraEnabled(cacheable=true)
  public static List<Contact> getContacts() {
      //return [SELECT Id, FirstName, LastName, Email FROM Contact WITH SECURITY_ENFORCED ORDER BY Name]; 
      throw new AuraHandledException('Forced error');
  }

  @AuraEnabled
  public static List<Account> getAccounts(){
    return [SELECT Id, Name FROM Account LIMIT 10];
  }

  @AuraEnabled
  public static List<Account> getRecords(String str){
    List<Account> accList = null;
    if(str.equals('Account')){
      accList = [SELECT Id, Name FROM Account LIMIT 10];
    }else if(str.equals('Contact')){
      //return [SELECT Id, Name FROM Contact LIMIT 10];
    }else if(str.equals('Opportunities')){
      //return [SELECT Id, Name FROM Opportunity LIMIT 10];
    }
    return accList;
  }



  @AuraEnabled
  public static List<Account> getSearchesAccounts(String searchKey){
    String key = '%' + searchKey + '%';
    if(key.equals('%%')){
      return null;
    }else{
      return [SELECT Id, Name FROM Account  WHERE Name LIKE :key LIMIT 10];
    }
  }
}