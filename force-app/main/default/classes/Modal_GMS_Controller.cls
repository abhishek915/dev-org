public class Modal_GMS_Controller {
    
    
    @AuraEnabled
    public static Opportunity getOptyDetails(Id optyId){
        Opportunity optyRec = [SELECT Id, Name FROM Opportunity WHERE Id= :optyId];
        return optyRec;
    }
    
    @AuraEnabled
    public static List<Product2> getProducts(){
        List<Product2> prodList = [SELECT Id, Name FROM Product2];
        return prodList;
    }
}