public class queryContactClass {
    
    @AuraEnabled
    public static List<Contact> getContacts(){
        return [SELECT Id, FirstName, LastName, Title, Account.Name, MailingCountry  FROM Contact WHERE Title != '' AND Account.Name != '' AND MailingCountry != '' ];
    }
    
    @AuraEnabled
    public static List<Account> getConsultingFirms(){
        return [SELECT Id, Name, BillingCity, BillingCountry FROM Account WHERE BillingCity != '' AND BillingCountry != '' LIMIT 10];
    }
    
    @AuraEnabled
    public static AccountContactRelation relateContactToAccount(String accountId ,String contactId){
        system.debug('in relateContactToAccount ---'+accountId+'----'+contactId);
        AccountContactRelation cn = new AccountContactRelation(AccountId = accountId, ContactId = contactId);
        insert cn;
        return cn;
        
    }
    
    @AuraEnabled
    public static AccountContactRelation findAndRelateContact(String parentAccountId, String accountId ,String contactId){
        system.debug('in relateContactToAccount --->>'+parentAccountId+'---'+accountId+'----'+contactId);
        AccountContactRelation cn = new AccountContactRelation();
        boolean removeDelReturn = removeAccountAssociation(parentAccountId, accountId);
        System.debug('removeDelReturn>>>'+removeDelReturn);
        if(removeDelReturn){
            cn = new AccountContactRelation(AccountId = parentAccountId, ContactId = contactId);
            insert cn;
        }
        
        return cn;
    }
    
    @AuraEnabled
    public static Partner relateAccountToAccount(String accFromId, String accToId){
        Partner linkAccounts = new Partner(AccountFromId = accFromId, AccountToId = accToId);
        insert linkAccounts;
        return linkAccounts;
    }
    
    @AuraEnabled
    public static boolean removeAccountAssociation(String accountFromId, String accountToId){
        Partner[] accRel = [SELECT AccountFromId, AccountToId FROM Partner WHERE AccountFromId =: accountFromId AND AccountToId =: accountToId];
        Database.DeleteResult[] deleteResults = Database.delete(accRel, false);
        system.debug(deleteResults[0].isSuccess());
        return deleteResults[0].isSuccess();
    }
}