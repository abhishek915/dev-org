public class ContactAndLeadSearch {

    public static List<List< SObject>> searchContactsAndLeads(String queryString){
       List<List<sObject>> searchList = [FIND :queryString IN ALL FIELDS 
                   RETURNING Contact(Name),Lead(Name)];
		
        return searchList;
    }
}