@isTest
public class LeadProcessorTest {
    
    
    @testSetup 
    static void setup() {
        List<Lead> lead = new List<Lead>();
        for (Integer i=0;i<200;i++) {
            lead.add(new Lead(LastName='Lead No- '+i, FirstName='Test', Company='Test Lead Company'));
        }
        insert lead;
    }
    
    static testmethod void test() {        
        Test.startTest();
        LeadProcessor lPro = new LeadProcessor();
        Id batchId = Database.executeBatch(lPro);
        Test.stopTest();
        
        // after the testing stops, assert records were updated properly
        System.assertEquals(200, [select count() from Lead where LeadSource = 'Dreamforce']);
    }
    
    
}