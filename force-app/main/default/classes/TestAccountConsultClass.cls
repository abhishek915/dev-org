@isTest
public class TestAccountConsultClass {
    
    @isTest
    static void TestGetPrimaryConsultantId(){
        String primaryAccId = AccountConsultClass.getPrimaryConsultantId('0012800000lRQPKAA4');
    }
    
    @isTest
    static void TestRetrieveAllContacts(){
        List<Account> accRecords = AccountConsultClass.retrieveAllContacts('0012800000lRQPKAA4');
    }
    
    @isTest
    static void TestGetPartnerAccounts(){
        List<Account> partnerAccRecords = AccountConsultClass.getPartnerAccounts('0012800000lRQPKAA4');
    }
    
    @isTest
    static void TestGetAccountRelatedContacts(){
        List<Contact> conRecords = AccountConsultClass.getAccountRelatedContacts('0012800000lRQPKAA4');
    }
    
    @isTest
    static void TestGetPartnerAccountDetails(){
        Account acc = AccountConsultClass.getPartnerAccountDetails('0012800000lRQPKAA4');
    }

    @isTest
    static void TestDeleteRelationship(){
        Test.startTest();
        boolean querySuccess = AccountConsultClass.deleteRelationship('0012800000lRQPKAA4', '07k28000002FxlYAAS');
        Test.stopTest();
    }
    
	@isTest
    static void TestGetRelatedAccountId(){
        String accId = AccountConsultClass.getRelatedAccountId('07k28000002FxlYAAS');
    }
    
    @isTest
    static void TestUpdatePrimaryId(){
        Test.startTest();
        List<Account> accPrimaryIdDetail = AccountConsultClass.updatePrimaryId('0012800000lRQPOAA4', '0032800000y5QrQAAU');
        Test.stopTest();
    }
}