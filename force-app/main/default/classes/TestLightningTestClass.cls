@isTest public class TestLightningTestClass {

    @isTest static void TestQuery(){
        LightningTestClass.getContact('0032800000W7YqbAAF');
    }
    
    @isTest static void TestUpdate(){
        String accountId = '0032800000W7YqbAAF';
        Test.startTest();
        List<Contact> con = new List<Contact>();
        Contact contact = new Contact(LastName='Levvy', Id=accountId);
        con.add(contact);
        LightningTestClass.updateContact(con, accountId);
        Test.stopTest();
    }
}