public class LightningTestClass {
    
    @AuraEnabled
    public static List<Contact> getContact(String contactId){
        //system.debug(contactId);
        return [SELECT FirstName,LastName,Phone,Title,Languages__c,Email,Fax,MailingCountry,Birthdate FROM Contact WHERE Id =:contactId];
    }
    
    
   /* public static void insertContact(){
        List<Contact> contList = new List<Contact>();
        contList = [SELECT FirstName, LastName FROM Contact LIMIT 5];
        system.debug('contactListrbc- -- -:::'+contList);
        
        List<Contact> contactToCreate = new List<Contact>();
        contactToCreate = [Contact:{FirstName='Johnny', LastName='Walker'}, Contact:{FirstName='Carol', LastName='Ruiz'}];
 
		String [] names = new List<String>();
        names.add('Kumar');
        names.add('Singh');
        names.add('Pandey');
        
        for(Integer i = 0; i<names.size(); i++){
            String n = names[i];
            Contact con = new Contact(LastName=n);
        	system.debug('contact details'+con);
            insert con;
        }
        
       // Contact con = new (Contact:{FirstName='Abhishek', LastName='Kumar'}, Contact:{FirstName='Abhishek', LastName='Singh'});
		
    //	insert con;
    }
    */
    
    @AuraEnabled
    public static List<Contact> updateContact(List<Contact> con, String accountId){
        //system.debug('====>'+con);
        
      
        update con;
        
        //update con;        
     /*   List<Contact> contactList = [SELECT Id FROM Contact WHERE AccountId =:accountId ];
        if(!contactList.isEmpty()){
            System.debug('not empty');
          
            
        } */
        
        return con;
    }
}