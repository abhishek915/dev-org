public with sharing class AccountController {
  public static List<Account> getAllActiveAccounts() {
    return [SELECT Id,Name,Active__c FROM Account WHERE Active__c = 'Yes'];
  }
  
  public static List<Account> getAllActiveAccounts1() {
    return [SELECT Id,Name,Active__c FROM Account WHERE LIMIT 10];
  }
}