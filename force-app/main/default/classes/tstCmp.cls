public class tstCmp{
    
    @AuraEnabled
    public static Map<Id,Contact> getContacts(){
        Map<Id,Contact> conMap = new Map<Id,Contact>([SELECT Id,FirstName,LastName,Phone,Email FROM Contact LIMIT 5]);
        
        return conMap;
    }
}