public class VisitRelatedQueries {
    
    public static List<Visit__c> getVisitDetails(){
        return [SELECT Name, Visit_Type__c,  Visit_Date_Time__c From Visit__c ];
    }
    
    @AuraEnabled
    public static List<Account> getAssociatedAccounts(String accountId){
        List<Account> allAccountsList = new List<Account>();
        
        List<AccountContactRelation> acRecords = [SELECT ContactId FROM AccountContactRelation WHERE AccountId=: accountId AND IsDirect=true];
        for(AccountContactRelation acRecord : acRecords){
            List<AccountContactRelation> accR = getAccountContactRelation(acRecord.ContactId);
            
            for(AccountContactRelation accRecord : accR){
                Account acc = getAccountDetails(accRecord.AccountId);
                allAccountsList.add(acc);
            }
        }
        
        system.debug('all List >>'+allAccountsList);
        return allAccountsList;
    }
    
    @AuraEnabled
    public static List<OpportunityContactRole> getOpportunities(String accountId){
        List<String> contactIdList = new List<String>();
        List<OpportunityContactRole> optyList = new List<OpportunityContactRole>();
        List<AccountContactRelation> acRecords = [SELECT ContactId FROM AccountContactRelation WHERE AccountId=: accountId AND IsDirect=true];
        for(AccountContactRelation acRecord : acRecords){
            contactIdList.add(acRecord.ContactId);
        }        
        
        for(String contactId : contactIdList){
        	//System.debug('contactId >>> >> >'+contactId); 
            List<OpportunityContactRole> opty = getRelatedOpportunities(contactId);
            //System.debug('opty...>>'+opty);
            optyList.addAll(opty);
            //System.debug('optyList..>>>'+optyList);
        }
        
        return optyList;
        
    }
    
    
    @AuraEnabled
    public static List<AccountContactRelation> getAccountContactRelation(String conId){
        List<AccountContactRelation> ac = [SELECT AccountId, ContactId FROM AccountContactRelation WHERE ContactId=: conId AND IsDirect=false];
        return ac;
    }
    
    @AuraEnabled
    public static Account getAccountDetails(String accountId){
        return [SELECT Id, Name, Type, Owner.Name FROM Account WHERE Id=: accountId];
    }
    
    @AuraEnabled
    public static List<OpportunityContactRole> getRelatedOpportunities(String conId){
        //List<Account> acc = [SELECT Id, (SELECT Contact.Id FROM AccountContactRelations WHERE IsDirect=true) FROM Account WHERE Id =:accountId];
        //List<AccountContactRelation> acc = [SELECT ContactId FROM AccountContactRelation WHERE AccountId =: accountId AND IsDirect=true];
        //System.debug('>>>----'+conId);
        
        List<OpportunityContactRole> optyList = [SELECT OpportunityId, ContactId, Opportunity.Name, Opportunity.StageName, Opportunity.CloseDate, Contact.Name FROM OpportunityContactRole WHERE ContactId=: conId];
        //System.debug('opty>>'+optyList);
        return optyList;
    }
    
}