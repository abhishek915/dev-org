public class FindPrimeNumber {
    
    public static String getAccountName(Id optyId){
        Opportunity opp = [SELECT Name, AccountId FROM Opportunity WHERE Id=: optyId];
        System.debug('>>>'+opp.AccountId);
        return opp.AccountId;
    }
    
    
    public static String checkForOddOrEven(Integer n){
        String message = '';
        if(n>50){
            message = 'Even Number';
        }
        if(n<50){
            message = 'Odd Number';
        }
        
        return message;
    }

}