public class createAccountRecords {
    @AuraEnabled
    public static List<String> getPickList(String objName,String fldName) {
        List<String> pkList = new List<String>();Map<String,Schema.SObjectType> allObj = Schema.getGlobalDescribe();
        Map<String,Schema.SObjectField> allFlds = allObj.get(objName).getDescribe().fields.getMap();
        List<Schema.PicklistEntry> pickList = allFlds.get(fldName).getDescribe().getPickListValues();
        for(Schema.PicklistEntry pk : pickList){
            pkList.add(pk.getValue());
        }
        return pkList;
    }
    
    @AuraEnabled
    public static Account createAccount(Account ac){
        HTTP h = new HTTP();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/ef0a7993-0160-4f68-8573-a075d70202ce/v3/translate?version=2018-05-01');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        String username = 'apikey';
        String password = 'MVcC14O3cU5s3Yow8BT5enD39d9n0LnXUQg_SpW_atiJ';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        req.setBody('{"text": ["Welcome to IBM Watson"], "model_id":"en-fr"}');
        
        HTTPResponse res = h.send(req);
        System.debug(res.getBody());
        
        insert ac;
        return ac;
    }
    
    public static void GetTranslation(){
        
    }
}