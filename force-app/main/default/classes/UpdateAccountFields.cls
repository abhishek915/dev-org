global class UpdateAccountFields implements Database.Batchable<sObject>{
    global final String Query;
    global final String Entity;
    global final String Field;
    global final String Value;
    
    /*String q = 'SELECT Website FROM Account WHERE Name LIKE \'%Bulk Company\' ORDER BY Name LIMIT 20';
    String e = 'Account';
    String f = 'Website';
    String v = 'www.crmitsfdc.com';
    Id batchInstanceId = Database.executeBatch(new UpdateAccountFields(q,e,f,v)); */
    
    global UpdateAccountFields(String q, String e, String f, String v){
        Query=q; Entity=e; Field=f;Value=v;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, 
                        List<sObject> scope){
                            System.debug('List of accounts >>> '+scope);
                            
                            for(Sobject s : scope){s.put(Field,Value); 
                                                  }      update scope;
                        }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}