global class Batch1OnContact implements Database.Batchable<sObject>{
    
    private String query;
    
    global Batch1OnContact(String soql){
        query = soql;
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String fN = 'Batch';
        //String query = 'SELECT Id FROM Contact WHERE FirstName LIKE :fN';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> conRecords){
        System.debug('conRecords-->>'+conRecords.size());
        for(Contact c : conRecords){
            c.Batch_field__c = 'Updated From Batch 5';
        }
        
        try{
            update conRecords;
        }catch(Exception e){
            System.debug('Exception-->>'+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 
        System.debug('>>>> finish ' + job.Status);
        
        System.debug('Batch finished.');
    }
}