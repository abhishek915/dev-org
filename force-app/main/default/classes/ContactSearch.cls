public class ContactSearch {
    
    public static List<Contact> searchForContacts(String lName, String mailingPIN){
        List<Contact> contactList = [SELECT Id, Name, LastName, MailingPostalCode
                                    FROM Contact
                                    WHERE LastName = :lName AND MailingPostalCode = :mailingPIN];
        
        return contactList;
    }
}