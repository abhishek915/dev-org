public class AccountsController {
  @AuraEnabled
  public static List<Contact> getContacts() {
    return [SELECT Id, Name, email, Phone FROM Contact ORDER BY Name DESC LIMIT 10];
  }
}