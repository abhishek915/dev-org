public with sharing class DemoQueryFieldSetData {
    @AuraEnabled
    public static List<FieldSetMember> getFields(String typeName, String fsName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        System.debug('targetType'+targetType);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        System.debug('describe'+describe);
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Map<String, Schema.SObjectField> field_map = describe.fields.getMap();
        System.debug('fsMap'+fsMap);
        Schema.FieldSet fs = fsMap.get(fsName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetMember> fset = new List<FieldSetMember>();
        for (Schema.FieldSetMember f: fieldSet) {
             List<String> option = new List<String>();
            if(f.getType() == field_map.get(f.fieldPath).getDescribe().getType()){
                List<Schema.PicklistEntry> pick_list_values = field_map.get(f.fieldPath).getDescribe().getPickListValues();
                for (Schema.PicklistEntry a : pick_list_values) {
                    option.add(a.getValue()); //add the value and label to our final list
                }
                fset.add(new FieldSetMember(f,option));
            }else{
                option.add(null);
                fset.add(new FieldSetMember(f,option));
            }
            
        }
        return fset;
    }
    
    public class FieldSetMember {
        
        public FieldSetMember(Schema.FieldSetMember f,List<String> opt) {
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required;
            this.type = '' + f.getType();
            this.picklistoptions = opt;
        }
        
        public FieldSetMember(Boolean DBRequired) {
            this.DBRequired = DBRequired;
        }
        
        @AuraEnabled
        public Boolean DBRequired { get;set; }
        
        @AuraEnabled
        public String fieldPath { get;set; }
        
        @AuraEnabled
        public String label { get;set; }
        
        @AuraEnabled
        public Boolean required { get;set; }
        
        @AuraEnabled
        public String type { get; set; }
        
        @AuraEnabled
        public List<String> picklistoptions { get; set;}
    }
}