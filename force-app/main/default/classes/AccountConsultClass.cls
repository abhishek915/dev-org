public class AccountConsultClass {
    
    public static List<Account> method1(){
        return [SELECT Id, Name FROM Account LIMIT 10];
    }


    @AuraEnabled
    public static String getPrimaryConsultantId(String accountId){
        String primaryConsultantId = '';
        List<Account> acc = [SELECT Primary_Consultant_Id__c FROM Account WHERE Id=: accountId];
        
        if(acc[acc.size()-1].Primary_Consultant_Id__c != null || acc[acc.size()-1].Primary_Consultant_Id__c != ''){
            primaryConsultantId = acc[acc.size()-1].Primary_Consultant_Id__c;
        }
        return primaryConsultantId;
    }
    
    @AuraEnabled
    public static List<Account> retrieveAllContacts(String accountId){
        List<Account> acc = [SELECT Id, (SELECT IsDirect, Contact.Id, Contact.FirstName, Contact.LastName, Contact.Title, Contact.MailingCountry, Contact.Account.Name, Contact.Account.Id FROM AccountContactRelations WHERE IsDirect=false) FROM Account WHERE Id =:accountId];
        return acc;
    }
    
    @AuraEnabled
    public static List<Account> getPartnerAccounts(String accountId){
        List<Account> accDetailsList = new List<Account>();
        List<Partner> par = [SELECT AccountFromId, AccountToId FROM Partner WHERE AccountFromId=: accountId];
        for(Integer i = 0; i < par.size(); i++ ){
            String accId = par.get(i).AccountToId;
            Account accList = getPartnerAccountDetails(accId);   
            accDetailsList.add(accList);
        }
        return accDetailsList; 
    }
    
    @AuraEnabled
    public static List<Contact> getAccountRelatedContacts(String accountId){
        return [SELECT Id, FirstName, LastName, Title, Account.Name, MailingCountry  FROM Contact WHERE AccountId=: accountId];
    }
    
    @AuraEnabled
    public static Account getPartnerAccountDetails(String accountId){
        return [SELECT Name FROM Account WHERE Id= :accountId];   
        //return acc;
    }
    
    @AuraEnabled
    public static boolean deleteRelationship(String accountId, String recordId){
        boolean deleteSuccess = false;
        
        Id recId = Id.valueOf(recordId);
        
        Schema.SObjectType sobjectType = recId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        system.debug('object type ==  >>'+sobjectName);
        
        if(sobjectName.equalsIgnoreCase('AccountContactRelation')){
            String accId = getRelatedAccountId(recordId);
            system.debug('deleting contactrelation'+accId);
            AccountContactRelation[] conRel = [SELECT AccountId, ContactId FROM AccountContactRelation WHERE Id=: recordId];
            Database.DeleteResult[] deleteResults = Database.delete(conRel, false);
            deleteSuccess = deleteResults[0].isSuccess();
            if(deleteSuccess == true){
                system.debug('inside deletesuccess check');
                queryContactClass.relateAccountToAccount(accountId, accId);
            }
        }
        
        else if(sobjectName.equalsIgnoreCase('Account')){
            system.debug('deleting accountrelation');
            Partner[] accRel = [SELECT AccountFromId, AccountToId FROM Partner WHERE AccountFromId =:accountId AND AccountToId =: recordId];
            Database.DeleteResult[] deleteResults = Database.delete(accRel, false);
            deleteSuccess = deleteResults[0].isSuccess();
        }
        return deleteSuccess;
    }
    
    public static String getRelatedAccountId(String recId){
        String accountId = '';
        System.debug('recId--'+recId);
        AccountContactRelation acR = [SELECT AccountId, ContactId FROM AccountContactRelation WHERE Id=: recId];    
        String contactId = acR.ContactId;
        System.debug('<<'+contactId);
        Contact con = [SELECT AccountId FROM Contact WHERE Id=: contactId];
        system.debug('acc Id ===>>'+con.AccountId);
        accountId = con.AccountId;
        return accountId;
    }
    
    @AuraEnabled
    public static List<Account> updatePrimaryId(String accountId, String recordId){
        List<Account> acc = [SELECT Primary_Consultant_Id__c FROM Account WHERE Id=: accountId];
        acc[0].Primary_Consultant_Id__c = recordId;
        update acc;
        return acc;
    }
    
}