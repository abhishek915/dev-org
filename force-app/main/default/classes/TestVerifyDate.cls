@isTest
public class TestVerifyDate {

    @isTest static void TestCheckDates(){        
        Date D1 = VerifyDate.CheckDates(Date.valueOf(system.today()),Date.valueOf(system.today()).addDays(29));
        system.assertEquals(Date.valueOf(system.today()).addDays(29), D1);
        
        Date D2 = VerifyDate.CheckDates(Date.valueOf(system.today()),Date.valueOf(system.today()).addDays(31));
        system.assertEquals(Date.newInstance(system.today().year(), system.today().month(), Date.daysInMonth(system.today().year(), system.today().month())), D2);        
        
    }
}