import { LightningElement, wire, track, api} from 'lwc';
import getContacts from '@salesforce/apex/ContactController.getContacts'
//import getAccounts from '@salesforce/apex/ContactController.getAccounts'
import getRecords from '@salesforce/apex/ContactController.getRecords'

export default class contactsLWC extends LightningElement {
    @wire(getContacts) contacts;
    //@wire(getAccounts) accounts;
    @track fetchedList;
    @track error;
    @api isLoaded = false;
    @api errorOccured = false;

    @track searchKey = '';
    @track btnName = '';

    //@wire(getSearchesAccounts, { searchKey : '$searchKey'})
    //accounts;
    
    loadData(event){
        var name1 = event.target.name;
        this.btnName = name1;

        this.isLoaded = true;
        this.fetchedList = [];
        //getAccounts()
        getRecords(name1)
        .then(result => {
            this.fetchedList = result;
            this.errorOccured = false;
            this.isLoaded = false;
        })
        .catch(error => {
            this.error = error;
            this.errorOccured = true;
            this.isLoaded = false;
        })
    }

    getKeyedInContact(event){
        const searchKey = event.target.value;
        this.searchKey = searchKey;
    }
}