import { LightningElement, track } from 'lwc';

export default class interest_calculator_lwc extends LightningElement {

    @track prinicpalAmount = 0;
    @track rateOfInterest = 0;
    @track periodOfTime = 0;
    @track accuredAmount = 0;


    onPAmtChange(event){
        this.prinicpalAmount= parseFloat(event.target.value);
    }

    onRateChange(event){
        this.rateOfInterest = parseFloat(event.target.value);
    }

    onPeriodChange(event){
        this.periodOfTime = parseFloat(event.target.value);
    }

    calculateInterest(){
        this.accuredAmount = this.prinicpalAmount+((this.prinicpalAmount * this.periodOfTime * this.rateOfInterest)/100);
    }

}