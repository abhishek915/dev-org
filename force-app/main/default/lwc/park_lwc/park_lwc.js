import { LightningElement,wire,api } from 'lwc';
import getImageUrl from '@salesforce/apex/park_lwc_controller.getImageUrl'

export default class ParkAnimals extends LightningElement {

    @api recordId;
    @wire(getImageUrl,{parkRecId:'$recordId'}) imageUrls;
}