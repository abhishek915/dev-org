import { LightningElement, track, wire } from 'lwc';
import getContactsMethod from '@salesforce/apex/getContactsLWC.getContactsMethod';

//const DELAY = 300;

export default class fetchContactsDynamically extends LightningElement {
    @track searchKey = '';

    @wire(getContactsMethod, { searchKey : '$searchKey'})
    contacts;

    getKeyedInContact(event){
      //window.clearTimeout(this.delayTimeout);
      const searchKey = event.target.value;
      this.searchKey = searchKey;
      /*this.delayTimeout = setTimeout(() => {
          this.searchKey = searchKey;
      }, DELAY);*/
    }
}