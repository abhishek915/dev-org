({
	doInit : function(component, event, helper) {
		
	},
    
    readCards : function(component, event, helper){
        
        var selectedRec = event.currentTarget;
        var dept = selectedRec.dataset.record;
        
        var checkedUnchecked = selectedRec.checked;
        
        var cmpEvent = component.getEvent("getUpdatedCards");
        cmpEvent.setParams({"competitorsData": component.get('v.competitorsArray')});
        cmpEvent.setParams({"departMent": dept});
        cmpEvent.setParams({"addRemove" : checkedUnchecked});
        cmpEvent.fire();
    }
})