({
    getContactRecordsForAccounts : function(component) {
        var action = component.get('c.getAccountRelatedContacts');
        var accountId = component.get('v.accountId');
        console.log("accountId>>>>"+accountId);
        action.setParams({"accountId" : accountId});
        action.setCallback(this, function(response){
            //alert("response"+JSON.stringify(response.getReturnValue()));
            component.set('v.contacts', response.getReturnValue());
        });
        $A.enqueueAction(action);
    }   
})