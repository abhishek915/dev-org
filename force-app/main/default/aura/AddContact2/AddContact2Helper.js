({
    relateRecords : function(component, contactId) {
        
        var action = component.get('c.relateContactToAccount');
        var accountId = component.get('v.accId');
        
        action.setParams({"accountId" : accountId, "contactId" : contactId});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Contact added successfully!!"
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);	
    },
    
    relateAccounts : function(component, accToId){
        var action = component.get('c.relateAccountToAccount');
        var accFromId = component.get('v.accId');
        
        action.setParams({"accFromId" : accFromId, "accToId" : accToId});
        action.setCallback(this, function(response){
            console.log(response.getState());
            if(response.getState() === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Account added successfully!!"
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    }
})