({
	addContactToAccount : function(component, event, helper) {
		var selectedItem = event.currentTarget;
     	var recordId = selectedItem.dataset.record;
        
        helper.relateRecords(component, recordId);
	},
    
    
    addAccountToAccount : function(component, event, helper) {
		var selectedItem = event.currentTarget;
     	var recordId = selectedItem.dataset.record;
        
        helper.relateAccounts(component, recordId);
	}
})