({
	doInit : function(component, event, helper) {
        helper.getContactRecords(component);
    },
    
    getUpdatedList : function(component, event, helper){
        var selectVal = component.find("selectConsultant").get("v.value");
        
        if(selectVal == 'Consultants'){

            helper.getContactRecords(component);
        }else{

        	helper.getUpdatedListOfRecords(component);    
        }
        
    }
})