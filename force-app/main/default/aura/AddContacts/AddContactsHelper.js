({
    getContactRecords : function(component) {
        var action = component.get('c.getContacts');
        var self = this;
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                /*var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Queried Successfully."
                });
                toastEvent.fire();*/
                component.set('v.allContacts', response.getReturnValue());
               // $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    getUpdatedListOfRecords : function(component){
        var action = component.get('c.getConsultingFirms');
        action.setCallback(this, function(response){
            console.log(JSON.stringify(response.getReturnValue()));
            var state = response.getState();
            
            if(state === "SUCCESS"){
                component.set('v.allContacts', response.getReturnValue());
                //$A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    }
})