({
	doInit : function(component, event, helper) {
        /*var AccountTeamData = [{"LastName":"Byrne","FirstName":"Mathew","JobTitle":"Regional Vice President","Role":"CM RVP","Email":"byrneMathew@uhc.com","ReportsTo":""},{"LastName":"Doten","FirstName":"Steve","JobTitle":"Sales Vice President","Role":"CD SVP","Email":"StevenDoten@uhc.com","ReportsTo":""},{"LastName":"McCurdy-Spence","FirstName":"Kimberly","JobTitle":"Strategic Client Executive","Role":"CM SCE","Email":"KimberlySpence@uhc.com","ReportsTo":""},{"LastName":"schultz","FirstName":"Steven","JobTitle":"Sales Vice President","Role":"UNA Sr Executive","Email":"StevenSchultz","ReportsTo":""},{"LastName":"Anderson","FirstName":"Lisa","JobTitle":"System Administrator","Role":"CD SVP","Email":"merit@uhc.com","ReportsTo":""}];
        component.set("v.AccountTeam",AccountTeamData);
        component.set("v.ord",1); */
        helper.getUsers(component);
    },
    
    addSelectedComponent : function(component, event, helper) {
        var recId = event.getParam('UserSearchArr');
        /*var selectedObj = '';
       component.get("v.AccountTeam").map(function(obj, index){
            if(divId === index){
                selectedObj = obj.Name;
            }
        });*/
        var cmpEvent = component.getEvent("addAccountTeamEvent");
        cmpEvent.setParams({"selectedUser": recId});
        cmpEvent.fire();
    },
    
    searchUsers : function(component, event, helper){
        
        helper.searchUsersUsingFilter(component);
        
    },
    
    clearSearchFilterField : function(component, event, helper){
        component.find('searchKeywordForUsers').set('v.value', '');
    }
})