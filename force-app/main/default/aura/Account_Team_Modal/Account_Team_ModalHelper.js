({
	getUsers : function(component) {
		var action = component.get('c.getAllUsers');
        action.setCallback(this, function(response){
                //console.log(JSON.stringify(response.getReturnValue()));
				component.set('v.AccountTeam', response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    
    searchUsersUsingFilter : function(component){
        var field = component.find("FieldsDropDown").get("v.value");
        var filter = component.find("FilterDropDown").get("v.value");
        var searchKeyword = component.find('searchKeywordForUsers').get('v.value');
        
        var action = component.get('c.getUsersUsingFilter');
        action.setParams({"field" : field, "filter" : filter, "keyword" : searchKeyword});
        action.setCallback(this, function(response){
            component.set('v.AccountTeam', response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    }
})