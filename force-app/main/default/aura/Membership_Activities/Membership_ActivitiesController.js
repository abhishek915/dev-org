({
	doInit : function(component, event, helper) {
		var membershipActivitiesData = [{'Name':'Yelp','Sales_Stage_Medical':'Notified','Sales_Stage_Pharmacy':'Notified','Sales_Stage_Dental':'Notified','Sales_Stage_Vision':'Notified','Sales_Stage_Programs':'Notified','Effective_Date':'01/01/2017','Existing_Medical_Members':'0','Estimated_Additional_New_Medical_Members':'3975','Sold_Medical_Members':'0','Estimated_Medical_Membership_At_Risk':'0','Actual_Medical_Membership_Loss':''},{'Name':'Yelp','Sales_Stage_Medical':'Notified','Sales_Stage_Pharmacy':'Notified','Sales_Stage_Dental':'Notified','Sales_Stage_Vision':'Notified','Sales_Stage_Programs':'Notified','Effective_Date':'01/01/2017','Existing_Medical_Members':'0','Estimated_Additional_New_Medical_Members':'3975','Sold_Medical_Members':'0','Estimated_Medical_Membership_At_Risk':'0','Actual_Medical_Membership_Loss':''},{'Name':'Yelp','Sales_Stage_Medical':'Notified','Sales_Stage_Pharmacy':'Notified','Sales_Stage_Dental':'Notified','Sales_Stage_Vision':'Notified','Sales_Stage_Programs':'Notified','Effective_Date':'01/01/2017','Existing_Medical_Members':'0','Estimated_Additional_New_Medical_Members':'3975','Sold_Medical_Members':'0','Estimated_Medical_Membership_At_Risk':'0','Actual_Medical_Membership_Loss':''}]
        component.set('v.membershipActivitiesDataArray',membershipActivitiesData);
    },

    expandCollapse: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var divId = selectedItem.dataset.record;      
        var cmpTarget = component.find(divId);
        $A.util.toggleClass(cmpTarget,'slds-is-open');
        var iconElement = selectedItem.getAttribute("id");
        
        var myLabel = component.find(iconElement).get("v.iconName");
        
        if(myLabel=="utility:chevronright"){
            component.find(iconElement).set("v.iconName","utility:chevrondown");
        }else if(myLabel=="utility:chevrondown"){
            component.find(iconElement).set("v.iconName","utility:chevronright");
        }
    },
    
    newOpty : function(component, event, helper){
        console.log(component.get('v.recordId'));
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues": {
                'AccountId' : component.get('v.recordId'),
                'StageName' : 'Needs Analysis'
            }
        });
        createRecordEvent.fire();
    },
    
    display : function(component, event, helper) {
        helper.toggleHelper(component, event);
    },
    
    displayOut : function(component, event, helper) {
        helper.toggleHelper(component, event);
    }
})