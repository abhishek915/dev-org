({
    doInit : function(component, event, helper) {
        var action = component.get("c.getContacts");//Fetching Map from Controller
        action.setCallback(this, function(a) {
            component.set("v.contacts", a.getReturnValue());//Set return value to Contact Map
            var contacts = component.get("v.contacts");//Getting Map and Its value
             
            var tile='<ul class="slds-list--vertical slds-has-cards">';
             
            for (var key in contacts){
                tile += '<li class="slds-list__item">';
                tile += '<div class="slds-tile slds-tile--board">';
                tile += '<div class="slds-tile__detail">';
                tile += '<p class="slds-text-heading--medium">'+ contacts[key].FirstName+' '+ contacts[key].LastName +'</p>';
                tile += '<p class="slds-truncate">Email - '+ contacts[key].Email +'</p>';
                tile += '<p class="slds-truncate">Phone - '+ contacts[key].Phone +'</p>';
                tile += '</div>';
                tile += '</div>';
                tile += '</li>';
            }
            tile += '</ul>';
            document.getElementById("contactDetail").innerHTML = tile;            
        });
        $A.enqueueAction(action);
    }
})