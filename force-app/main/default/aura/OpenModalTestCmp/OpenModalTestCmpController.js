({
    openModal : function(component, event, helper){
        $A.createComponents([["c:ModalCmp",{attribute:true,'Modalheader':'Account Details','ModalBodyData':{'accountId':component.get('v.recordId')},'ModalBody':'AccountDetailsCmp'}]],
                            function(newCmp, status){ 
                                if (component.isValid() && status === 'SUCCESS') {
                                    var dynamicComponentsByAuraId = {};
                                    for(var i=0; i < newCmp.length; i++) {
                                        var thisComponent = newCmp[i];
                                        dynamicComponentsByAuraId[thisComponent.getLocalId()] = thisComponent;
                                    }
                                    component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                                    component.set("v.dynamicComponentAuraId", thisComponent.getLocalId()); 
                                    component.set("v.body", newCmp); 
                                } 
                            });
    },
    
    modelCloseComponentEvent : function(component, event,helper) {
        helper.modalGenericClose(component);
    }
})