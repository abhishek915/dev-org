({
    modalGenericClose : function(component) {
        var modalComponent = component.get("v.dynamicComponentsByAuraId");
        modalComponent = modalComponent[component.get("v.dynamicComponentAuraId")];
        modalComponent.modalClosing();
        component.set("v.dynamicComponentsByAuraId", {});
        component.set("v.dynamicComponentAuraId", '');
        component.set("v.body", []);
    }
})