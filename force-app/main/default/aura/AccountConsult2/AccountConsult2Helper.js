({
    deleteRelation : function(component, recordId) {
        var action = component.get('c.deleteRelationship');
        var accountId = component.get('v.accountId');
        //alert(recordId)
        
        action.setParams({"accountId" : accountId, "recordId" : recordId});
        action.setCallback(this, function(response){
            
            if(response.getReturnValue() === true){
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!!",
                    "message": "De-association successful."
                });
                toastEvent.fire();
                
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    updatePrimaryId : function(component, recordIdToUpdate){
        var accountId = component.get('v.accountId');
        var action = component.get('c.updatePrimaryId');
        action.setParams({"accountId" : accountId, "recordId" : recordIdToUpdate});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!!",
                    "message": "Primary Consultant Updated."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    getConsultants : function(component, accId){
        var action = component.get('c.getAccountRelatedContacts');
        action.setParams({"accountId" : accId});
        action.setCallback(this, function(response){
            alert(JSON.stringify(response.getReturnValue()));
        });
        $A.enqueueAction(action);
    }
})