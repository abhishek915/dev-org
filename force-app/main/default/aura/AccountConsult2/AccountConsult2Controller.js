({
    navToRecord : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.record;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    },
    
    savePrimary : function(component, event, helper){
        component.set('v.showSave', false);
        var oldPrimaryId = component.get('v.primaryId');
        var selectedItem = event.currentTarget;
        var selectedRecordId = selectedItem.dataset.record;
        if(oldPrimaryId != selectedRecordId){
            helper.updatePrimaryId(component, selectedRecordId);
        }
    },
    
    findConsultants : function(component, event, helper){
        var selectedItem = event.currentTarget;
        var rowAccountId = selectedItem.dataset.record;
        
        component.set("v.body", []);
        var parAccount = component.get('v.accountId');
        var modalComponent = component.find('find_Contact_Modal');
        for(var i in modalComponent){
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.addClass(modalComponent[i], 'slds-backdrop--open');
        }
        
        $A.createComponents([["c:FindContacts",{attribute:true, "parentAccount" : parAccount, "accountId" : rowAccountId}]],
                            function(newCmp, status){ 
                                
                                if (component.isValid() && status === 'SUCCESS') {
                                    
                                    component.set("v.body", newCmp); 
                                } 
                            });
    },
    
    modalClose : function(component, event, helper){
        component.set("v.body", []);
        var modalComponent = component.find('find_Contact_Modal');
        for(var i in modalComponent){
            $A.util.addClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--open');
        }
    },
    
    editPrimary : function(component, event, helper) {
        
        component.set('v.showSave', true);
        
        /*var toggleLink = component.find("text");
        $A.util.toggleClass(toggleLink, "toggle");
        
        //component.find('saveLink').set('v.disabled', 'false');
        
         var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.record;
        //alert("-----------------"+recordId);
        var checkPrimaryAccount = component.find(recordId+"1");
        console.log("--------------"+checkPrimaryAccount);
        checkPrimaryAccount.set("v.disabled","false");
        
        /*var checkPrimaryAccount = component.find('primaryAccount');
        
        for(var i=0; i<checkPrimaryAccount.length; i++){
            checkPrimaryAccount[i].set("v.disabled","false");
        }*/
    },
    
    remove : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.record;
        
        helper.deleteRelation(component, recordId);
    }	
})