({
	doInit : function(component, event, helper){
        helper.getOptyDetails(component);
    },
    
    navToRecord : function (component, event, helper) {
        
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.record;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    }
})