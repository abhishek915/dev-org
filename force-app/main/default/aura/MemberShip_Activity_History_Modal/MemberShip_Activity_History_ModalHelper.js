({
	getOptyDetails : function(component) {
        var accId = component.get('v.recordId');
      	
        var action = component.get('c.getOpportunities');
        if(accId != null || accId.length >= 0){
        	action.setParams({"accountId" : accId});    
        }
        
        action.setCallback(this, function(response){
            var listOfOpportunities = response.getReturnValue();
            console.log(JSON.stringify(listOfOpportunities));
            component.set('v.accountOpportunities', listOfOpportunities);
        });
        $A.enqueueAction(action);
    }
})