({
    getAccountTeam : function(component){
        var recId = component.get('v.recordId');
        
        var action = component.get('c.getAccountTeamMembers');
        action.setParams({"accountId" : recId});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set('v.AccountTeamData', response.getReturnValue());    
            }else{
                console.log("Failed with state: " + response.getState());
            }
            
        });
        $A.enqueueAction(action);
    },
    
    deleteUser : function(component, event){
        var accountId = component.get('v.recordId');
        var userId = component.get('v.AccountTeamIndex');
        
        var action = component.get('c.deleteUserFromAccountTeam');
        action.setParams({"accountId" : accountId, "userId" : userId});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS" && response.getReturnValue() == true){
                this.showToast('User Deleted Successfully!!');
            }else{
                console.log("Failed with state: " + response.getState());
            }            
        });
        $A.enqueueAction(action);
        
        this.getAccountTeam(component);
    },
    
    addUserToTeam : function(component){
        var accountId = component.get('v.recordId');
        var userId = component.get('v.AccountTeamIndex');
        
        var action = component.get('c.addUserToAccountTeam');
        action.setParams({"accountId" : accountId, "userId" : userId});
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS" && response.getReturnValue() == true){
                this.showToast('User Added Successfully!!');
            }else{
                console.log("Failed with state: " + response.getState());
            }
        });
        $A.enqueueAction(action);
        
        this.getAccountTeam(component);
    },
    
    showToast : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": msg
        });
        toastEvent.fire();
    },
    
    globalCloseModal : function(component) {
        var consultantLookUp = component.find("ConsultantLookUp");
        var consultantmodal = component.find("consultantmodal");
        $A.util.addClass(consultantLookUp,'display');
        $A.util.addClass(consultantmodal,'display');
        $A.util.removeClass(consultantLookUp,'slds-modal slds-fade-in-open slds-modal--large');
        $A.util.removeClass(consultantmodal,'slds-backdrop slds-backdrop--open');
    },
    
    modalGenericClose : function(component) {
        var device = $A.get("$Browser.formFactor");
        if(device == "DESKTOP"){
            var modalComponent = component.get("v.dynamicComponentsByAuraId");
            modalComponent = modalComponent[component.get("v.dynamicComponentAuraId")];
            modalComponent.modalClosing();
            component.set("v.dynamicComponentsByAuraId", {});
            component.set("v.dynamicComponentAuraId", ''); 
        }else{
            var modalComponent = component.get("v.dynamicComponentsByAuraId");
            modalComponent = modalComponent[component.get("v.dynamicComponentAuraId")];
            modalComponent.modalClosing();
            component.set("v.dynamicComponentsByAuraId", {});
            component.set("v.dynamicComponentAuraId", ''); 
        }
    }
})