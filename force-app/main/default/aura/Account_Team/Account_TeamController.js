({
    doInit : function(component, event, helper) {
        /*var action = component.get("c.AccountTeamApplet");
        action.setCallback(this,function(response){
      	  var state = response.getState();
        	if(component.isValid() && state=== "SUCCESS"){
               console.log("response.getReturnValue(): " + response.getReturnValue());
               component.set("v.AccountTeamData", response.getReturnValue()); 
               console.log("AccountTeamData: " + component.get("v.AccountTeamData"));
            }else{
                console.log("Failed with state: " + state);
            }
      	});
    $A.enqueueAction(action);*/        
        var AccountTeam = [{"LastName":"Byrne","FirstName":"Mathew","JobTitle":"Regional Vice President","UserRole":"CM RVP","WorkPhone":"1(631)348-5636"},{"LastName":"Doten","FirstName":"Steve","JobTitle":"Sales Vice President","UserRole":"CD SVP","WorkPhone":"1(407)659-7118"},{"LastName":"McCurdy-Spence","FirstName":"Kimberly","JobTitle":"Strategic Client Executive","UserRole":"CM SCE","WorkPhone":"1(770)300-3557"},{"LastName":"schultz","FirstName":"Steven","JobTitle":"Sales Vice President","UserRole":"CD SVP","WorkPhone":"1(312)424-5718"}];
        //component.set("v.AccountTeamData",AccountTeam);
        helper.getAccountTeam(component);
    },
    
    Account_Team_Applet : function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        
        var recordId = component.get('v.recordId');
        if(recordId == null || recordId.length == 0 ){
            recordId = "";
        }
        
        if(device == "DESKTOP"){
            $A.createComponents([["c:Modal_Component",{attribute:true, "recordId" : recordId,'Modalheader':'Search for a User','ModalBody':'Account_Team_Modal'}]],
                                function(newCmp, status){ 
                                    if (component.isValid() && status === 'SUCCESS') {
                                        var dynamicComponentsByAuraId = {};
                                        for(var i=0; i < newCmp.length; i++) {
                                            var thisComponent = newCmp[i];
                                            dynamicComponentsByAuraId[thisComponent.getLocalId()] = thisComponent;
                                        }
                                        component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                                        component.set("v.dynamicComponentAuraId", thisComponent.getLocalId()); 
                                        component.set("v.body", newCmp); 
                                    } 
                                });
        }else{
            
            $A.createComponents([["c:Panel_Component",{attribute:true,'Modalheader':'Search for a User','ModalBody':'Account_Team_Modal'}]],
                                function(newCmp, status){ 
                                    if (component.isValid() && status === 'SUCCESS') {
                                        var dynamicComponentsByAuraId = {};
                                        for(var i=0; i < newCmp.length; i++) {
                                            var thisComponent = newCmp[i];
                                            dynamicComponentsByAuraId[thisComponent.getLocalId()] = thisComponent;
                                        }
                                        component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                                        component.set("v.dynamicComponentAuraId", thisComponent.getLocalId()); 
                                        component.set("v.body", newCmp); 
                                    } 
                                });
            
        }
    },
    
    expandCollapse: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var divId = selectedItem.dataset.record;      
        var cmpTarget = component.find(divId);
        $A.util.toggleClass(cmpTarget,'slds-is-open');
        var iconElement = selectedItem.getAttribute("id");
        
        var myLabel = component.find(iconElement).get("v.iconName");
        
        if(myLabel=="utility:chevronright"){
            component.find(iconElement).set("v.iconName","utility:chevrondown");
        }else if(myLabel=="utility:chevrondown"){
            component.find(iconElement).set("v.iconName","utility:chevronright");
        }
    },
    modalClose : function(component, event, helper) {
        var modalComponent = component.find('Account_Team_Modal');
        for(var i in modalComponent){
            $A.util.addClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--open');
        }
    },
    addSelectedAccountTeam : function(component, event, helper) {
        helper.modalGenericClose(component);
        if(event.getParam('clicked') == 'cancel'){
            var modalComponent = component.find('Account_Team_Modal');
            for(var i in modalComponent){
                $A.util.addClass(modalComponent[i], 'slds-hide');
                $A.util.removeClass(modalComponent[i], 'slds-show');
            }
        }else{
            var recId = event.getParam('selectedUser');
        	component.set('v.AccountTeamIndex', recId);
            
            helper.addUserToTeam(component);
            
            /*var accountArray = component.get("v.AccountTeamData");
            accountArray.push({"LastName":"Anderson","FirstName":"Lisa","JobTitle":"System Administrator","UserRole":"CD SVP","WorkPhone":"1(631)348-5636"});
            component.set("v.AccountTeamData",accountArray);   */
        }
    },
    confirmDelete: function(component, event, helper) {
        var deleteAcc = component.find('AccountTeam');
        for(var i in deleteAcc){
            $A.util.removeClass(deleteAcc[i], 'slds-show');
            $A.util.addClass(deleteAcc[i], 'slds-hide');
        }
        helper.deleteUser(component);
      /*  var recordId =  component.get('v.AccountTeamIndex');
        //divId = parseInt(divId)
        
        var consultingHistoryArray = helper.;
        component.get("v.AccountTeamData").map(function(obj, index){
            if(divId !== index){
                consultingHistoryArray.push(obj);
            }
        });
        console.log(consultingHistoryArray);
        component.set("v.AccountTeamData",consultingHistoryArray);*/
    },
    confirmCancel: function(component, event, helper) {
        var deleteAcc = component.find('AccountTeam');
        for(var i in deleteAcc){
            $A.util.removeClass(deleteAcc[i], 'slds-show');
            $A.util.addClass(deleteAcc[i], 'slds-hide');
        }
        
    },
    removeRecords: function(component, event, helper) {
        var recId = event.getParam('AccountTeamIndx');
        component.set('v.AccountTeamIndex', recId);
        var deleteAcc = component.find('AccountTeam');
        for(var i in deleteAcc){
            $A.util.removeClass(deleteAcc[i], 'slds-hide');
            $A.util.addClass(deleteAcc[i], 'slds-show');
        }
    }
})