({
	getFieldSetData : function(cmp,event,helper) {
        helper.getFsetData(cmp,event);
    },
    
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("Record is loaded successfully.");
            component.set('v.simpleRecord',component.get('v.simpleRecord'));
            
        } else if(eventParams.changeType === "CHANGED") {
            component.find('recordLoader').reloadRecord();
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
    
    saveRecord: function(component, event, helper) {
        component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {            
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful             
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)            
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {                
                // handle component related logic in event handler                
            } else if (saveResult.state === "INCOMPLETE") {                
                console.log("User is offline, device doesn't support drafts.");                
            } else if (saveResult.state === "ERROR") {                
                
            }
            
        }));        
    }
})