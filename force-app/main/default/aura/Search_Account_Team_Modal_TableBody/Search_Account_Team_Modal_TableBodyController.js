({
	select : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var selectedItemId = selectedItem.dataset.record;
        
		var cmpEvent = component.getEvent("searchAccountTeamModalTableEvent");
        cmpEvent.setParams({"UserSearchArr": selectedItemId});
        cmpEvent.fire();
	}
})