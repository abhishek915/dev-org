({
	getFieldSetList : function(component) {
		var action = component.get('c.getFieldSetFields');
        action.setParams({"accountId" : component.get('v.recordId')});
        action.setCallback(this, function(response){
            component.set('v.fieldSetFieldsList', response.getReturnValue());
        });
        $A.enqueueAction(action);
	}
})