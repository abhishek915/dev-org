({
    doInit : function(component, event, helper){
        if($A.get("$Browser.formFactor") === 'DESKTOP'){
            setTimeout(function(){ 
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();    
            }, 500);
        }
        
        
    },
    
    openPopup : function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        
        var recordId = component.get('v.recordId');
        if(recordId == null || recordId.length == 0 ){
            recordId = "";
        }
        
        if(device == "DESKTOP"){
            $A.createComponents([["c:Modal_Component",{attribute:true, "recordId" : recordId,'Modalheader':'Search for a Competitor','ModalBody':'Component_Search_PopUp2'}]],
                                function(newCmp, status){ 
                                    if (component.isValid() && status === 'SUCCESS') {
                                        var dynamicComponentsByAuraId = {};
                                        for(var i=0; i < newCmp.length; i++) {
                                            var thisComponent = newCmp[i];
                                            dynamicComponentsByAuraId[thisComponent.getLocalId()] = thisComponent;
                                        }
                                        component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                                        component.set("v.dynamicComponentAuraId", thisComponent.getLocalId()); 
                                        component.set("v.body", newCmp); 
                                    } 
                                });
        }else{
            
            $A.createComponents([["c:Panel_Component",{attribute:true,'Modalheader':'Search for a Competitor','ModalBody':'Component_Search_PopUp2'}]],
                                function(newCmp, status){ 
                                    if (component.isValid() && status === 'SUCCESS') {
                                        var dynamicComponentsByAuraId = {};
                                        for(var i=0; i < newCmp.length; i++) {
                                            var thisComponent = newCmp[i];
                                            dynamicComponentsByAuraId[thisComponent.getLocalId()] = thisComponent;
                                        }
                                        component.set("v.dynamicComponentsByAuraId", dynamicComponentsByAuraId);
                                        component.set("v.dynamicComponentAuraId", thisComponent.getLocalId()); 
                                        component.set("v.body", newCmp); 
                                    } 
                                });
            
        }
    },
    
    checks : function(component, event, helper){
        var co = component.find("b1");
        
        for(var i in co){
            co[i].set('v.disabled', true);
        }
    }
})