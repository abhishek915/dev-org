({
    doInit : function(component, event, helper) {
        var modalComponent = component.find('contact_History_Modal');
        for(var i in modalComponent){
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.addClass(modalComponent[i], 'slds-backdrop--open');
        }
        var recordId = component.get('v.recordId');
        if(recordId == null || recordId.length == 0 ){
            recordId = "";
        }
        $A.createComponents([["c:"+component.get('v.ModalBody'),{attribute:true, "recordId" : recordId}]],
                            function(newCmp, status){ 
                                if (component.isValid() && status === 'SUCCESS') { 
                                    component.set("v.body", newCmp); 
                                } 
                            });
    },
    
    modalClose : function(component, event, helper) {  
        component.set("v.body", []);
        var modalComponent = component.find('contact_History_Modal');
        for(var i in modalComponent){
            $A.util.addClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--open');
        }
    },
})