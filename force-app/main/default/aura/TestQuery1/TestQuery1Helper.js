({
	getDetails : function(component) {
		
        var action = component.get('c.getAssociatedAccounts');
        action.setParams({"accountId" : '0012800001IvKFxAAN'});
        action.setCallback(this, function(response){
            console.log(JSON.stringify(response.getReturnValue()));
            var listOfAccounts = response.getReturnValue();
            component.set('v.listAccounts', listOfAccounts);
        });
        $A.enqueueAction(action);
	}
})