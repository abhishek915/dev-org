({
    handleSuccess : function (cmp, event, helper) {
        var editRecordEvent = $A.get("e.force:createRecord");
        editRecordEvent.setParams({
            "entityApiName": "Contact"
        });
        editRecordEvent.fire();  
    },    
    
    handleError: function (cmp, event, helper) {
        cmp.find('notifLib').showToast({
            "title": "Something has gone wrong!",
            "message": event.getParam("message"),
            "variant": "error"
        });
    }
})