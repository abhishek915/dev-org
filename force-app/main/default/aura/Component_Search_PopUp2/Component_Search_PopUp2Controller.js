({
    doInit : function(component, event, helper){
        
        component.set('v.topCRecords', [{'Competitor':'BMS Health','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'},{'Competitor':'UHG Insurance','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'}]);        
        component.set('v.bluesRecords', [{'Competitor':'UHG Insurance','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'}]);
        component.set('v.exchangeRecords', [{'Competitor':'Suvidha Health','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'}]);
        component.set('v.otherRecords', [{'Competitor':'BMS Health','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'},{'Competitor':'Insurans Limited','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'},{'Competitor':'Suvidha Health','Pharmacy':'false','Dental':'false','Vision':'false','Others':'false'}]);
        
        component.set("v.tableRecords", component.get('v.topCRecords'));
    },
    
    saveRecords : function(component, event, helper){
        //alert('save');
    },
    
    cancelPopup : function(component, event, helper){
        //alert('cancel');
    },
    
    clearSearchFilterField : function(component, event, helper){
        
    },
    
    showCards : function(component, event, helper){
        
        var dataRec = event.getParam("competitorsData");
        var department = event.getParam("departMent");
        var addRemove = event.getParam("addRemove");
        
        if(addRemove != null && addRemove) {
            
            var dataRecord = {"compName": dataRec.Competitor, "name" : department};
            
            if(department == 'Pharmacy') {
                var pharmacyData = component.get('v.allPharmacyCards');
                pharmacyData.push(dataRecord);
                component.set('v.allPharmacyCards', pharmacyData);    
            }
            if(department == 'Dental') {
                var dentalData = component.get('v.allDentalCards');
                dentalData.push(dataRecord);
                component.set('v.allDentalCards', dentalData);    
            }
            if(department == 'Vision') {
                var visionData = component.get('v.allVisionCards');
                visionData.push(dataRecord);
                component.set('v.allVisionCards', visionData);    
            }
            if(department == 'Others') {
                var otherData = component.get('v.allOtherCards');
                otherData.push(dataRecord);
                component.set('v.allOtherCards', otherData);    
            }
            
            var tabRecords = component.get('v.tableRecords');
            for(var l in tabRecords){
                if(tabRecords[l].Competitor == dataRec.Competitor){
                    tabRecords[l][department] = true;
                    component.set('v.tableRecords', tabRecords);
                    if(component.get('v.selectedOption') == 'Top Competitors'){
                        component.set('v.topCRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'The Blues and their Affiliates'){
                        component.set('v.bluesRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'Exchange Competitors'){
                        component.set('v.exchangeRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'Other Competitors'){
                        component.set('v.otherRecords', tabRecords);
                    }
                }
            }
            
        } else if(!addRemove) {
            
            if(department == 'Pharmacy'){
                var dataRecordsCollection = component.get('v.allPharmacyCards');
            }
            if(department == 'Dental'){
                var dataRecordsCollection = component.get('v.allDentalCards');
            }
            if(department == 'Vision'){
                var dataRecordsCollection = component.get('v.allVisionCards');
            }
            if(department == 'Others'){
                var dataRecordsCollection = component.get('v.allOtherCards');
            }
            
            for(var i in dataRecordsCollection){
                if((dataRecordsCollection[i].compName == dataRec.Competitor) && (dataRecordsCollection[i].name == department)){
                    dataRecordsCollection.splice(i, 1);
                    
                    if(department == 'Pharmacy'){
                        component.set('v.allPharmacyCards', dataRecordsCollection);
                    }
                    if(department == 'Dental'){
                        component.set('v.allDentalCards', dataRecordsCollection);
                    }
                    if(department == 'Vision'){
                        component.set('v.allVisionCards', dataRecordsCollection);
                    }
                    if(department == 'Others'){
                        component.set('v.allOtherCards', dataRecordsCollection);
                    }
                }
            }
            var tabRecords = component.get('v.tableRecords');
            for(var l in tabRecords){
                if(tabRecords[l].Competitor == dataRec.Competitor){
                    tabRecords[l][department] = false;
                    component.set('v.tableRecords', tabRecords);
                    
                    if(component.get('v.selectedOption') == 'Top Competitors'){
                        component.set('v.topCRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'The Blues and their Affiliates'){
                        component.set('v.bluesRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'Exchange Competitors'){
                        component.set('v.exchangeRecords', tabRecords);
                    }
                    if(component.get('v.selectedOption') == 'Other Competitors'){
                        component.set('v.otherRecords', tabRecords);
                    }
                }
            }
        }
    },
    
    removeDataRecCard : function(component, event, helper) {
        
        var selectedItem = event.currentTarget;
        var competitorName = selectedItem.dataset.record;
        var compDepartment = selectedItem.name;
        
        if(compDepartment == 'Pharmacy'){
            var dataRecordsCollection = component.get('v.allPharmacyCards');
        }
        if(compDepartment == 'Dental'){
            var dataRecordsCollection = component.get('v.allDentalCards');
        }
        if(compDepartment == 'Vision'){
            var dataRecordsCollection = component.get('v.allVisionCards');
        }
        if(compDepartment == 'Others'){
            var dataRecordsCollection = component.get('v.allOtherCards');
        }
        
        for(var i in dataRecordsCollection) {
            
            if((dataRecordsCollection[i].compName == competitorName) && 
               		(dataRecordsCollection[i].name == compDepartment)) {
                
                dataRecordsCollection.splice(i, 1);
                
                if(compDepartment == 'Pharmacy'){
                    component.set('v.allPharmacyCards', dataRecordsCollection);
                }
                if(compDepartment == 'Dental'){
                    component.set('v.allDentalCards', dataRecordsCollection);
                }
                if(compDepartment == 'Vision'){
                    component.set('v.allVisionCards', dataRecordsCollection);
                }
                if(compDepartment == 'Others'){
                    component.set('v.allOtherCards', dataRecordsCollection);
                }
                
                var tableDataRecords = component.get('v.tableRecords');
                
                for(var j in tableDataRecords) {
                    
                    if(tableDataRecords[j].Competitor == competitorName){
                        tableDataRecords[j][compDepartment] = false;
                        component.set('v.tableRecords', tableDataRecords);
                        
                            if(component.get('v.selectedOption') == 'Top Competitors'){
                                component.set('v.topCRecords', tableDataRecords);
                            }
                            if(component.get('v.selectedOption') == 'The Blues and their Affiliates'){
                                component.set('v.bluesRecords', tableDataRecords);
                            }
                            if(component.get('v.selectedOption') == 'Exchange Competitors'){
                                component.set('v.exchangeRecords', tableDataRecords);
                            }
                            if(component.get('v.selectedOption') == 'Other Competitors'){
                                component.set('v.otherRecords', tableDataRecords);
                            }
                    }
                }
            }
        }
    },
    
    searchUsers : function(component, event, helper) {
        
    },
    
    showOptions: function(component, event, helper) {

        console.log('clicked');
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.record;
        
        component.set('v.optionHeader', recordId);
        
        if(recordId == 'Top Competitors'){
            component.set("v.tableRecords", component.get('v.topCRecords'));
            component.set('v.selectedOption', recordId);
        }
        if(recordId == 'The Blues and their Affiliates'){
            component.set("v.tableRecords", component.get('v.bluesRecords'));
            component.set('v.selectedOption', recordId);
        }
        if(recordId == 'Exchange Competitors'){
            component.set("v.tableRecords", component.get('v.exchangeRecords'));
            component.set('v.selectedOption', recordId);
        }
        if(recordId == 'Other Competitors'){
            component.set("v.tableRecords", component.get('v.otherRecords'));
            component.set('v.selectedOption', recordId);
        }
        
        var arr = [];
        arr = component.find("listItems").getElement().childNodes;
        for(var cmp in component.find("listItems").getElement().childNodes) {
            $A.util.removeClass(arr[cmp], "selectedRow");
        }
        
        var targetElement = event.target;
        $A.util.addClass(targetElement, "selectedRow");
    }
})