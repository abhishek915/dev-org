({
	remove: function(component, event, helper) {        
        var selectedRecord = event.currentTarget;
        var selectedRecordId = selectedRecord.dataset.record;
        
        var cmpEvent = component.getEvent("Account_Team_Table");
        cmpEvent.setParams({"AccountTeamIndx": selectedRecordId});
        cmpEvent.fire();        
    }
})