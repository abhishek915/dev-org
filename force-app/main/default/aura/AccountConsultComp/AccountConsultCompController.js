({
    doInit : function(component, event, helper) {
        helper.getAllContacts(component);
    },
    
    addContact : function(component, event, helper){
        component.set("v.body", []);
        var accountId = component.get('v.recordId');
        var modalComponent = component.find('add_Contact_Modal');
        for(var i in modalComponent){
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.addClass(modalComponent[i], 'slds-backdrop--open');
        }
        
        $A.createComponents([["c:AddContacts",{attribute:true, "accountId" : accountId}]],
                            function(newCmp, status){ 
                                
                                if (component.isValid() && status === 'SUCCESS') {
                                    
                                    component.set("v.body", newCmp); 
                                } 
                            });
    },
    
    modalClose : function(component, event, helper){
        component.set("v.body", []);
        var modalComponent = component.find('add_Contact_Modal');
        for(var i in modalComponent){
            $A.util.addClass(modalComponent[i], 'slds-backdrop--hide');
            $A.util.removeClass(modalComponent[i], 'slds-backdrop--open');
        }
    },
    
    createRec : function(component, event, helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues": {
                'AccountId' : '0012800001IvKFxAAN'
            }
        });
        createRecordEvent.fire();
    },
    
    demoFunction : function(component, event, helper){
    //var action = component.get('c.getVisits');        
    //action.setCallback(this, function(response) {
    //  console.log(">>>>>>>>"+JSON.stringify(response.getReturnValue()));
    //var state = response.getState();
    // if (state === "SUCCESS") {
    //  var toastEvent = $A.get("e.force:showToast");
    //  toastEvent.setParams({
    // 	  "title": "Success!",
    //  	  "message": "The record has been updated."
    // });
    // toastEvent.fire();
    // }
    //});
    //$A.enqueueAction(action); 
    //var createRecordEvent = $A.get("e.force:createRecord");
    //createRecordEvent.setParams({
    //   "entityApiName": "Account"
    //});
    //createRecordEvent.fire();
}
 })