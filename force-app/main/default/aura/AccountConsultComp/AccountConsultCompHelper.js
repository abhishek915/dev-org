({
    getAllContacts : function(component) {
        var arrOfAccounts = [];
        var listRecords = [];
        var arrOfAccountDetails = [];
        var primaryConsultantId = '';
        var action = component.get('c.getPrimaryConsultantId');
        var accountId = component.get('v.recId');
        action.setParams({"accountId" : accountId});
        var self = this;
        action.setCallback(this, function(response) {
            primaryConsultantId = response.getReturnValue();
            component.set('v.primaryId', primaryConsultantId);
        });
        $A.enqueueAction(action);
        
        var action2getContacts = component.get('c.retrieveAllContacts');
        action2getContacts.setParams({"accountId" : accountId});
        action2getContacts.setCallback(this, function(responseData){
            var state = responseData.getState();
            if(state === "SUCCESS"){
                var newList = [];
                if(responseData.getReturnValue()[0].AccountContactRelations){
                    listRecords = responseData.getReturnValue()[0].AccountContactRelations;  
                }
                //listRecords = responseData.getReturnValue()[0].AccountContactRelations;
                for(var i in listRecords){
                    var conId = listRecords[i].Contact.Id;
                    if(conId === primaryConsultantId){
                        listRecords[i].IsDirect = true; 
                    }else{
                        listRecords[i].IsDirect = false; 
                        
                    }
                }
                //component.set('v.contacts', listRecords);
                $A.get('e.force:refreshView').fire();
            }
        });
        
        $A.enqueueAction(action2getContacts);
        
        var actionToFetchAccounts = component.get('c.getPartnerAccounts');
        actionToFetchAccounts.setParams({"accountId" : accountId});
        actionToFetchAccounts.setCallback(this, function(response){
            var resp = response.getReturnValue();
            component.set('v.contacts', listRecords.concat(resp));
            /*if(listRecords.length != 0 && resp.length != 0){
                component.set('v.contacts', listRecords.concat(resp));
            }
            if(listRecords.length == 0){
                component.set('v.contacts', resp);
            }
            if(resp.length == 0){
                component.set('v.contacts', listRecords);
            }*/
        });
        $A.enqueueAction(actionToFetchAccounts);
        
    },
    
    getAccountDetails : function(component, accountsList){
        for(var i in accountsList){
            console.log("--------"+accountsList);
            var actionToFetchAccountDetails = component.get('c.getPartnerAccountDetails');
            actionToFetchAccountDetails.setParams({"accountId" : accountsList});
            actionToFetchAccountDetails.setCallback(this, function(response){
                
                console.log(">>>>>>>>>"+JSON.stringify(response.getReturnValue()));
                
            });
            $A.enqueueAction(actionToFetchAccountDetails);
        }
    }
})