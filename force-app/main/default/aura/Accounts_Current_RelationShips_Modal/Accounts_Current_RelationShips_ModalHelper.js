({
    getDetails : function(component) {
        var accId = component.get('v.recordId');
      	
        var action = component.get('c.getAssociatedAccounts');
        if(accId != null || accId.length >= 0){
        	action.setParams({"accountId" : accId});    
        }
        
        action.setCallback(this, function(response){
            var listOfAccounts = response.getReturnValue();
            console.log(JSON.stringify(listOfAccounts));
            component.set('v.accountRecords', listOfAccounts);
        });
        $A.enqueueAction(action);
    }
})