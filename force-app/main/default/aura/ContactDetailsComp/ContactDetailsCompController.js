({
	doInit : function(component, event, helper) {
		helper.getRelatedContacts(component);
	},
    
    insertCon : function(component, event, helper){
        helper.saveContacts(component);
    },
    
    cancelEditTable : function(component, event, helper) {
        component.find('editBtn').set('v.disabled', false);
        component.find('cancelBtn').set('v.disabled', true);
        component.find('saveBtn').set('v.disabled', true);
        
		var myLabel = component.find('accNumber');
            console.log(myLabel);
            for(var i=0; i<myLabel.length; i++){
                myLabel[i].set("v.disabled","true");
            }
        
	},
    
    saveNewDetails : function(component, event, helper){
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
        
        component.set('v.editingAttribute', false);      
       	component.find('saveBtn').set('v.disabled', true);
        component.find('cancelBtn').set('v.disabled', true);
        component.find('editBtn').set('v.disabled', false);
        
        var listContacts = component.get('v.contacts');
        
            var myLabel = component.find('accNumber');
            console.log(myLabel);
            for(var i=0; i<myLabel.length; i++){
                myLabel[i].set("v.disabled","true");
            }
            
            helper.saveContactDetails(component, event,listContacts);
       
    },
    
    editTable : function(component, event, helper){
        component.find('editBtn').set('v.disabled', true);
        component.find('cancelBtn').set('v.disabled', false);
        component.find("saveBtn").set("v.disabled", false);
        
        var myLabel = component.find('accNumber');
        console.log(myLabel);
        for(var i=0; i<myLabel.length; i++){
            myLabel[i].set("v.disabled","false");
        }
        
		component.set('v.editingAttribute', true);    
    }
})