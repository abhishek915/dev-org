({
    getRelatedContacts : function(component) {
        var action = component.get('c.getContact');
        var contactId = component.get('v.recId');
        action.setParams({"contactId" : contactId});
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set('v.contacts', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    
    saveContacts : function(component){
        var action = component.get('c.insertContact');
        alert();
        $A.enqueueAction(action);
    },
    
    getError : function(component) {
        	/*var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Email not valid!"
                });
                toastEvent.fire(); */
    },
    
    saveContactDetails : function(component,event,testC){
        var accessreq = component.get("v.contacts");
        var earlyList = component.get("v.contactsEarlyList");
        
        var action = component.get('c.updateContact');
        action.setParams({"con":accessreq, "accountId" : '0012800000lRQPIAA4'});
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
               // console.log("success -- "+JSON.stringify(earlyList));
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been updated successfully."
                });
                toastEvent.fire();
                component.set('v.contacts', response.getReturnValue());
                $A.get('e.force:refreshView').fire();
            }
            
            if(state === "ERROR"){
                //console.log(JSON.stringify(earlyList));
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "The record has not been updated."
                });
                toastEvent.fire();
               // component.set('v.contacts', earlyList);
            }
            
            
        });
        $A.enqueueAction(action);
    }
})