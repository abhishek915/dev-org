({
	getRelatedContacts : function(component) {
		var action = component.get('c.getContact');
        action.setParams({"accountId" : '0012800000lRQPIAA4'});
    	var self = this;
    	action.setCallback(this, function(actionResult) {
     	component.set('v.contacts', actionResult.getReturnValue());
    	});
    	$A.enqueueAction(action);
	},
    
    saveContactDetails : function(component, testC){
        var accessreq = component.get("v.contacts");
        var action = component.get('c.updateContact');
        action.setParams({"con":accessreq, "accountId" : '0012800000lRQPIAA4'});
        
        var self = this;
    	action.setCallback(this, function(actionResult) {
     	component.set('v.contacts', actionResult.getReturnValue());
    	});
        $A.enqueueAction(action);
    }
})