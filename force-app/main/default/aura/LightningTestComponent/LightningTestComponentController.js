({
	doInit : function(component, event, helper) {
		helper.getRelatedContacts(component);
	},
    
    saveNewDetails : function(component, event, helper){
        component.set('v.editingAttribute', false);      
       	component.find('saveBtn').set('v.disabled', true);
        component.find('editBtn').set('v.disabled', false);
        var listContacts = component.get('v.contacts');
        helper.saveContactDetails(component, listContacts);
        
    },
    
    editTable : function(component, event, helper){
        component.find('editBtn').set('v.disabled', true);
		component.set('v.editingAttribute', true);    
        var saveButton = component.find("saveBtn");
        saveButton.set("v.disabled", false);
    }
})