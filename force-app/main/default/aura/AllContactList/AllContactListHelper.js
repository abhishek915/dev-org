({
  // Fetch the accounts from the Apex controller
  getContactsList: function(component) {
    var action = component.get('c.getContacts');

    // Set up the callback
    var self = this;
    action.setCallback(this, function(actionResult) {
     component.set('v.contactsList', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
  }
})