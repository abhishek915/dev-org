({
    doInit : function(component, event, helper) {
        var seniorUhgExecutivesEngaged = [{"Roles":"Enterprise Executive Sponsor","Name":"Ryen Jhon","Responsibilities":"Test CM Account has requested that Steve serve as Executive Sponser Test CM Account has requested that Steve serve as Executive Sponser"},{"Roles":"Optum Executive Sponsor","Name":"Connly Michael","Responsibilities":"Overall UHG Executive Sponser"},{"Roles":"UHC Executive Sponsor","Name":"Rachele Green","Responsibilities":"Overall UHG Executive Sponser"},{"Roles":"Other","Name":"Matthew Byrne","Responsibilities":"Regional VP"},{"Roles":"Other","Name":"","Responsibilities":""},{"Roles":"Other","Name":"","Responsibilities":""},{"Roles":"Other","Name":"","Responsibilities":""},{"Roles":"Other","Name":"","Responsibilities":""}];
    	component.set("v.seniorUhgExecutives",seniorUhgExecutivesEngaged);
    },
 	expandCollapse: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var divId = selectedItem.dataset.record;      
        var cmpTarget = component.find(divId);
        $A.util.toggleClass(cmpTarget,'slds-is-open');
        var iconElement = selectedItem.getAttribute("id");
        
        var myLabel = component.find(iconElement).get("v.iconName");
        
        if(myLabel=="utility:chevronright"){
            component.find(iconElement).set("v.iconName","utility:chevrondown");
        }else if(myLabel=="utility:chevrondown"){
            component.find(iconElement).set("v.iconName","utility:chevronright");
        }
    },
    saveExecutive: function(component, event, helper) {
        component.find("saveExecutive").set("v.disabled",true);
        component.find("editExecutive").set("v.disabled",false);
        $A.util.addClass(component.find("cancelExecutive"),"hideShow");
        var editTextAreas = component.find('editMode');
        var readModeAreas = component.find('readMode');
        for(var i in editTextAreas){
            $A.util.toggleClass(editTextAreas[i], 'readMode');
            $A.util.toggleClass(readModeAreas[i], 'editMode');
        }
    },
    editExecutive: function(component, event, helper) {
        component.find("editExecutive").set("v.disabled",true);
        component.find("saveExecutive").set("v.disabled",false);
        $A.util.removeClass(component.find("cancelExecutive"),"hideShow");
        var editTextAreas = component.find('editMode');
        var readModeAreas = component.find('readMode');
        for(var i in editTextAreas){
            $A.util.toggleClass(editTextAreas[i], 'readMode');
            $A.util.toggleClass(readModeAreas[i], 'editMode');
        }
    },
    cancelExecutive : function(component, event, helper) {
        component.find("saveExecutive").set("v.disabled",true);
        component.find("editExecutive").set("v.disabled",false);
        $A.util.addClass(component.find("cancelExecutive"),"hideShow");
    	var editTextAreas = component.find('editMode');
        var readModeAreas = component.find('readMode');
        for(var i in editTextAreas){
            $A.util.toggleClass(editTextAreas[i], 'readMode');
            $A.util.toggleClass(readModeAreas[i], 'editMode');
        }
    }
})