({
	relateContacts : function(component, contactId) {
		var parentAccountId = component.get('v.parentAccountId');
        var recordAccountid = component.get('v.accountId');
        var action = component.get('c.findAndRelateContact');
        action.setParams({"parentAccountId" : parentAccountId, "accountId" : recordAccountid, "contactId" : contactId});
        action.setCallback(this, function(response){
            if(response.getState() == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!!",
                    "message": "Contact association successful."
                });
                toastEvent.fire();
                
                $A.get('e.force:refreshView').fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!!",
                    "message": "Contact association failed."
                });
                toastEvent.fire();
                
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
	}
})