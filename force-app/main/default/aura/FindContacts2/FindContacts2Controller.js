({
	findContactAndAddToAccount : function(component, event, helper) {
		var selectedItem = event.currentTarget;
     	var rowContactId = selectedItem.dataset.record;
        
        helper.relateContacts(component, rowContactId);
	}
})