({
    getDetails : function(component) {
		var accId = component.get('v.recordId');
        
        var action = component.get('c.getAssociatedAccounts');
        action.setParams({"accountId" : accId});
        action.setCallback(this, function(response){
            var listOfAccounts = response.getReturnValue();
            component.set('v.accountHistoryDataArray', listOfAccounts);
        });
        $A.enqueueAction(action);
        
        var actionToGetOpty = component.get('c.getOpportunities');
        actionToGetOpty.setParams({"accountId" : accId});
        actionToGetOpty.setCallback(this, function(resp){
            component.set('v.membershipActivitiesDataArray', resp.getReturnValue());
        });
        $A.enqueueAction(actionToGetOpty);
        
	},
    
    toggleHelper : function(component,event) {
        var toggleText = component.find("tooltip1");
        $A.util.toggleClass(toggleText, "toggle");
    }
})