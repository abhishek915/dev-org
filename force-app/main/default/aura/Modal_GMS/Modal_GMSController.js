({
    init: function (component, event, helper) {
        var action = component.get('c.getOptyDetails');
        action.setParams({'optyId' : component.get('v.recordId')});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set('v.optyRec', response.getReturnValue());
            }else{
                console.log('error occurred!!');
            }
        });
        $A.enqueueAction(action);
    },
    
    openModal : function(component, event, helper) {
        component.set('v.isOpenModal', true);
        component.set('v.showSpinner', true);
        var action = component.get('c.getProducts');
        //action.setParams({ 'optyId' : component.get('v.recordId')});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                //console.log(JSON.stringify(response.getReturnValue()));  
                var returnProdList = response.getReturnValue();
                var productList = [];
                for(var i = 0; i<returnProdList.length; i++){
                    productList.push({ label: returnProdList[i].Name, value: returnProdList[i].Id });
                }
                //console.log('productList-->>'+productList);
                component.set('v.productList', productList);
            }else{
                console.log('error');
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
        if(component.get('v.screenNumber') == 1){
            //component.find('prevBtn').set('v.disabled', true);
        }
    },
    
    closeModal : function(component, event, helper) {
        component.set('v.screenNumber', 1);
        component.set('v.alreadySelectedvalues', []);
        component.set('v.isOpenModal', false);
    }, 
    
    prevScreen : function(component, event, helper) {
        component.set('v.showSpinner', true);
        setTimeout(function(){ 
            component.set('v.showSpinner', false);
        }, 2000);
        component.set('v.screenNumber', component.get('v.screenNumber')-1);
        if(component.get('v.screenNumber') == 1){
            component.set('v.alreadySelectedvalues', component.get('v.selectedProducts'));
            //component.find('prevBtn').set('v.disabled', true);
        }
        if(component.get('v.screenNumber') == 2){
            component.find('nxtBtn').set('v.disabled', false);
        }
        if(component.get('v.screenNumber') == 3){
            //component.find('nxtBtn').set('v.disabled', true);
        }else{
            //component.find('nxtBtn').set('v.disabled', false);
        }
        //event.setParam("value", component.get('v.selectedProducts');
    },
    
    nextScreen : function(component, event, helper) {
        component.set('v.showSpinner', true);
        setTimeout(function(){ 
            component.set('v.showSpinner', false);
        }, 2000);
        component.set('v.screenNumber', component.get('v.screenNumber')+1);
        if(component.get('v.screenNumber') == 2){
            //component.find('prevBtn').set('v.disabled', true);
            component.find('nxtBtn').set('v.disabled', false);
            
            component.set('v.updatedCount', 12);
            component.set('v.selectedItem', component.get('v.productListScreen2')[0].value);
            component.set('v.currentContent', component.get('v.productListScreen2')[0].label);
            component.set('v.screen2SelectedProduct', component.get('v.productListScreen2')[0].value);
        }else{
            //component.find('prevBtn').set('v.disabled', false);
        }
        if(component.get('v.screenNumber') == 3){
            // component.find('nxtBtn').set('v.disabled', true);
        }
    },
    
    handleChange: function (component, event) {
        var selectedProdIds = [];
        var selectedOptionValue = event.getParam("value");
        for(var i in selectedOptionValue){
            selectedProdIds.push(selectedOptionValue[i]);
        }
        component.set('v.selectedProducts', selectedProdIds);
        if(selectedProdIds.length > 0){
            component.find('nxtBtn').set('v.disabled', false);
        }else{
            component.find('nxtBtn').set('v.disabled', true);
        }
        
        var productListScreen2 = [];
        var productList = component.get('v.productList');
        for(var i in selectedProdIds){
            for(var j in productList){
                if(productList[j].value === selectedProdIds[i])
                    productListScreen2.push(productList[j]);
            }
        }
        //console.log('productListScreen2-->>'+JSON.stringify(productListScreen2));
        component.set('v.productListScreen2', productListScreen2);
    },
    
    handleSelect: function(component, event, helper) {
        var selected = event.getParam('name');
        component.set('v.screen2SelectedProduct', selected);
        
        var selectedProdname = null;
        var productList = component.get('v.productList');
        for(var i in productList){
            if(productList[i].value === selected)
                selectedProdname = productList[i].label;
        }
        component.set('v.currentContent', selectedProdname);
        
        //helper.getProdDetails(component, event, helper, selected);
    },
    
    saveQuoteData : function(component, event, helper) {
        alert('Saved successfully');
    }
})