({
    doInit : function(component, event, helper) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success);
            function success(position) {
                var lat = position.coords.latitude;
                alert(lat);
                var long = position.coords.longitude;
                alert(long);
            }
        } else {
            error('Geo Location is not supported');
        }
    }
})